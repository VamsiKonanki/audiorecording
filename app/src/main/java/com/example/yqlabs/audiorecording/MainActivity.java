package com.example.yqlabs.audiorecording;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "AudioRecord";
    private static String root = null;
    private static String audioPlayerName = null;
    private static Long millis;

    private LinearLayout homeLayout;
    private RelativeLayout recLayout;

    private Button audio_button = null;
    private Button audio_pic_button = null;

    private Button record_btn = null;
    private Button play_btn = null;
    private Button confirm_btn = null;

    private TextView counterTV, headerClose;
    private ProgressBar progressBar = null;

    private MediaRecorder recorder = null;
    private MediaPlayer mediaPlayer = null;

    private boolean isPlaying = false;
    private boolean isRecording = false;

    final int intervalTime = 30000; // 30 sec
    CountDownTimer timer;
    ObjectAnimator animation;

    /*You can put below codes in your project when you want to select audio.*/
    static final int PICK_AUDIO_REQUEST = 7;

    MediaPlayer mPlayer;
    Context con = null;
    SharedPreferences pref = null;
    TextView messageReceived;

    public MainActivity() {
        createDirectoriesIfNeeded();
        millis = Calendar.getInstance().getTimeInMillis();
        audioPlayerName = root + "/" + millis + "audio.mp3";
    }

    private void createDirectoriesIfNeeded() {

        root = Environment.getExternalStorageDirectory().getAbsolutePath();

        File folder = new File(root, "AudioRecord");

        if (!folder.exists()) {
            folder.mkdir();
        }

        File audioFolder = new File(folder.getAbsolutePath(), "Audio");

        if (!audioFolder.exists()) {
            audioFolder.mkdir();
        }

        root = audioFolder.getAbsolutePath();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.v("STATE", "onCreate");
        setContentView(R.layout.activity_main);

        try {
            con = createPackageContext(getResources().getString(R.string.shareAppPackage), Context.MODE_PRIVATE);//first app package name is "com.vamsi.retrofitxmlparsing"
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }

        messageReceived = (TextView) findViewById(R.id.messageReceived);

        setUpIds();

        setUpListeners();

    }

    @Override
    protected void onResume() {
        Log.v("STATE", "onResume");
        super.onResume();

        receiveShareAppData();

    }

    void receiveShareAppData() {

        try {
            Log.v("STATE", ".........");
            messageReceived.setText(".........");

            if (con == null)
                con = createPackageContext(getResources().getString(R.string.shareAppPackage), Context.MODE_PRIVATE);//first app package name is "com.vamsi.retrofitxmlparsing"

            /*pref = PreferenceManager.getDefaultSharedPreferences(con);*/

            if (pref == null)
                pref = con.getSharedPreferences(
                        getResources().getString(R.string.vamShareAppData), Context.MODE_PRIVATE);

            String shareAppData = "" + pref.getString(getResources().getString(R.string.shareAppData), "No Value");

            Log.v("STATE", ""+shareAppData);
            messageReceived.setText(shareAppData);

            shareAppData = null;
            con = null;
            pref = null;

        } catch (NameNotFoundException e) {
            Log.e("Not data shared", e.toString());
        }

    }

    private void setUpIds() {

        homeLayout = (LinearLayout) findViewById(R.id.homeLayout);
        recLayout = (RelativeLayout) findViewById(R.id.recLayout);

        audio_button = (Button) findViewById(R.id.audio_button);
        audio_pic_button = (Button) findViewById(R.id.audio_pic_button);

        record_btn = (Button) findViewById(R.id.record_btn);
        play_btn = (Button) findViewById(R.id.play_btn);
        confirm_btn = (Button) findViewById(R.id.confirm_btn);

        headerClose = (TextView) findViewById(R.id.headerClose);
        counterTV = (TextView) findViewById(R.id.counterTV);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        animation = ObjectAnimator.ofInt(progressBar, "progress", 0, 500);

        recLayout.setVisibility(View.GONE);

        counterTV.setText("0");

        counterTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (counterTV.getText().toString().equalsIgnoreCase("Play")) {
                    /*Play in default player*/
                    playInPlayer(audioPlayerName);
                }

            }
        });
        play_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Play in default player*/
                playInPlayer(audioPlayerName);

            }
        });

        confirm_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                homeLayout.setEnabled(true);
                homeLayout.setClickable(true);
                recLayout.setVisibility(View.GONE);

                if (timer != null) {
                    animation.cancel();
                    animation.end();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        animation.resume();
                    }
                    timer.cancel();
                    timer = null;
                }

                if (!audioPlayerName.isEmpty())
                    Toast.makeText(getApplicationContext(), audioPlayerName, Toast.LENGTH_LONG).show();
            }
        });

        headerClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                homeLayout.setEnabled(true);
                homeLayout.setClickable(true);
                recLayout.setVisibility(View.GONE);

                if (timer != null) {
                    animation.cancel();
                    animation.end();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        animation.resume();
                    }
                    timer.cancel();
                    timer = null;
                }

            }
        });

        audio_pic_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    Intent intent_upload = new Intent();
                    intent_upload.setType("audio/*");
//                    intent_upload.setAction(android.content.Intent.ACTION_VIEW);
//                    File file = new File("/sdcard/test.mp4");
//                    intent_upload.setDataAndType(Uri.fromFile(file), "audio/*");
                    intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(intent_upload, PICK_AUDIO_REQUEST);

                } catch (Exception e) {
                }

            }
        });
    }

    private String getRealPathFromURI(Context context, Uri contentUri) {
        String[] projection = {MediaStore.Audio.Media.DATA};
        CursorLoader loader = new CursorLoader(context, contentUri, projection, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    /*And override onActivityResult in the same Activity, as below */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PICK_AUDIO_REQUEST) {

            if (resultCode == RESULT_OK) {

                ///storage/emulated/0/AudioRecord/Audio/1479207981045audio.mp3
                //the selected audio.
                Uri uri = data.getData();

                /*Play in default player*/
                if (uri != null) {
//                    String uu = getRealPathFromURI(this, uri);
                    String aa = audioPlayerName;
                    audioPlayerName = uri.getPath();
                    playInPlayer(audioPlayerName);
//                    Toast.makeText(getApplicationContext(), String.valueOf(uri), Toast.LENGTH_LONG).show();

                    /*try {
                        if (mediaPlayer != null)
                            mediaPlayer = null;

                        mediaPlayer = new MediaPlayer();

                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        mediaPlayer.setDataSource(this, uri);
                        mediaPlayer.prepare();
                        mediaPlayer.start();


                    } catch (IOException e) {
                        mediaPlayer.reset();
                        e.printStackTrace();
                    }*/


                    //Play Sound Button
                    /*final View mrButton = findViewById(R.id.push_button);
                    mrButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if(mediaPlayer.isPlaying()){
                                mediaPlayer.pause();
                            }else{
                                mediaPlayer.start();
                            }
                        }*/


//                    Uri.parse("content://media/internal/audio/media/122")
//                    RingtoneManager.getRingtone(this, uri).play();



                   /* mPlayer = new MediaPlayer();
                    mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    try {
                        mPlayer.setDataSource(getApplicationContext(), uri);
                    } catch (IllegalArgumentException e) {
                        Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                    } catch (SecurityException e) {
                        Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                    } catch (IllegalStateException e) {
                        Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        mPlayer.prepare();
                    } catch (IllegalStateException e) {
                        Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                    }
                    mPlayer.start();*/
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setUpListeners() {

        /*Open Audio Dialog*/
        audio_button.setOnClickListener(openRecordView);

        /*Start record*/
        record_btn.setOnClickListener(recordClickListener);
//        play_btn.setOnClickListener(playClickListener);
    }

    private View.OnClickListener openRecordView = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            homeLayout.setEnabled(false);
            homeLayout.setClickable(false);
            recLayout.setVisibility(View.VISIBLE);
            confirm_btn.setEnabled(false);
            counterTV.setText("0");
        }
    };

    private View.OnClickListener recordClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            isRecording = !isRecording;

            onRecord(isRecording);

            record_btn.setText(isRecording ? R.string.stop_recording : R.string.start_recording);
            confirm_btn.setEnabled(!isRecording);

            if (isRecording) {
                headerClose.setVisibility(View.INVISIBLE);
                confirm_btn.setEnabled(false);
            } else {
                headerClose.setVisibility(View.VISIBLE);
                confirm_btn.setEnabled(true);
            }

        }
    };

    private View.OnClickListener playClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            isPlaying = !isPlaying;

            onPlay(isPlaying);

            play_btn.setText(isPlaying ? R.string.stop_playing : R.string.start_playing);
            record_btn.setEnabled(!isPlaying);
        }
    };

    private void onRecord(boolean start) {
        if (start) {
            startRecording();
        } else {
            stopRecording();
        }
    }

    private void onPlay(boolean start) {
        if (start) {
            startPlaying();
        } else {
            stopPlaying();
        }
    }

    private void startPlaying() {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(audioPlayerName);
            mediaPlayer.setOnCompletionListener(completionListener);
            mediaPlayer.prepare();
            mediaPlayer.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private MediaPlayer.OnCompletionListener completionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            stopPlaying();
        }
    };

    private void startRecording() {

        millis = Calendar.getInstance().getTimeInMillis();
        audioPlayerName = root + "/" + millis + ".mp3";//audio.mp3

        if (recorder != null)
            recorder = null;

        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        // ACC_ELD is supported only from SDK 16+.
        // You can use other encoders for lower vesions.
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);//MPEG_4
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC_ELD);

        recorder.setAudioSamplingRate(44100);
        recorder.setAudioEncodingBitRate(96000);
        recorder.setOutputFile(audioPlayerName);

        try {
            recorder.prepare();
            recorder.start();

            if (timer != null)
                timer = null;

            runThread();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void stopRecording() {

        if (recorder != null) {
            recorder.release();
            completionRecording();

            if (timer != null) {
                timer.cancel();
                timer.onFinish();
                timer = null;
            }
        }
    }

    private void stopPlaying() {
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            completionPlaying();
        }
    }

    private void reset() {
        isRecording = false;
        isPlaying = false;
    }

    private void completionRecording() {
        reset();
        record_btn.setText(R.string.start_recording);
        play_btn.setText(R.string.start_playing);
        play_btn.setEnabled(true);
    }

    private void completionPlaying() {
        reset();
        play_btn.setText(R.string.start_playing);
        record_btn.setEnabled(true);
    }

    @Override
    public void onPause() {
        Log.v("STATE", "onPause");
        super.onPause();

        stopRecording();

        stopPlaying();

    }

    @Override
    public void onStop() {
        Log.v("STATE", "onStop");
        super.onStop();

        stopRecording();

        stopPlaying();

    }

    @Override
    protected void onDestroy() {
        Log.v("STATE", "onDestroy");
        super.onDestroy();
    }

    @Override
    protected void onPostResume() {
        Log.v("STATE", "onPostResume");
        super.onPostResume();
    }

    @Override
    protected void onStart() {
        Log.v("STATE", "onStart");
        super.onStart();
    }


    @Override
    protected void onRestart() {
        Log.v("STATE", "onRestart");
        super.onRestart();
    }

    @Override
    public void onBackPressed() {
        Log.v("STATE", "onBackPressed");
        super.onBackPressed();
    }

    ///storage/emulated/0/AudioRecord/Audio/1479207981045audio.mp3
    void playInPlayer(String _audioPlayerName) {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        File file = new File(_audioPlayerName);
        intent.setDataAndType(Uri.fromFile(file), "audio/*");
        startActivity(intent);

        /*File file = new File(_audioPlayerName);

        try {
            Intent myIntent = new Intent(android.content.Intent.ACTION_VIEW);
            File afile = new File(file.getAbsolutePath());
            String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
            String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            myIntent.setDataAndType(Uri.fromFile(file), mimetype);
            startActivity(myIntent);
        } catch (Exception e) {
// TODO: handle exception
            String data = e.getMessage();
        }*/
    }

    private synchronized void runProgressBar(long duration) {

        if (animation != null)
            animation = null;

        animation = ObjectAnimator.ofInt(progressBar, "progress", 0, 500); // see this max value coming back here, we animale towards that value

        animation.setDuration(duration); //in milliseconds
        animation.reverse();
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();

    }

    private synchronized void runThread() {

        timer = new CountDownTimer(intervalTime, 100) {

            boolean flag = true;
            int pd = 0;

            public void onTick(long millisUntilFinished) {
                if (flag)
                    runProgressBar(intervalTime);

                counterTV.setText("" + (millisUntilFinished / 1000) + " Sec");
                pd = (int) (millisUntilFinished / 1000);
                flag = false;


            }

            public void onFinish() {
                flag = false;

                if (pd == 0 && isRecording) {
                    isRecording = !isRecording;

                    onRecord(isRecording);

                    record_btn.setText(isRecording ? R.string.stop_recording : R.string.start_recording);
                    confirm_btn.setEnabled(!isRecording);

                    if (isRecording) {
                        headerClose.setVisibility(View.INVISIBLE);
                        confirm_btn.setEnabled(false);
                    } else {
                        headerClose.setVisibility(View.VISIBLE);
                        confirm_btn.setEnabled(true);
                    }
                }

                counterTV.setText("Play");
                animation.cancel();
                animation.end();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    animation.resume();
                }

            }
        }.start();

    }

}